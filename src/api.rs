use axum::http::{StatusCode, Uri};

#[allow(clippy::unused_async)]
pub async fn fallback(uri: Uri) -> (StatusCode, String) {
    let code = StatusCode::NOT_FOUND;
    (code, format!("{uri}: {code}"))
}

#[allow(clippy::unused_async)]
pub async fn ping() -> &'static str {
    "Pong!"
}
