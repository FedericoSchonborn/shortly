#![warn(clippy::pedantic)]

mod api;
mod cli;

use std::{net::SocketAddr, sync::Arc};

use anyhow::Result;
use axum::{routing::get, Extension, Router};
use clap::Parser;
use sqlx::SqlitePool;
use tokio::net::TcpListener;
use tracing::info;

use cli::Cli;

#[derive(Clone)]
pub struct State {
    pub pool: Arc<SqlitePool>,
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().init();

    let cli = Cli::parse();

    info!("Connecting to database at {}...", cli.db_url);
    let pool = SqlitePool::connect(&cli.db_url).await?;

    let address = SocketAddr::from(([127, 0, 0, 1], cli.port));
    info!("Going to listen on {address}...");

    Ok(axum::serve(
        TcpListener::bind(address).await?,
        Router::new()
            .fallback(api::fallback)
            .route("/api/ping", get(api::ping))
            .layer(Extension(State {
                pool: Arc::new(pool),
            })),
    )
    .await?)
}
