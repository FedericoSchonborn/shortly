use clap::Parser;

#[derive(Debug, Parser)]
#[clap(name = "shortly", version, author, about)]
pub struct Cli {
    #[clap(short, long, default_value_t = 8080)]
    pub port: u16,
    #[clap(short, long, default_value = "sqlite::memory:")]
    pub db_url: String,
}
